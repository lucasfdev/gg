package gg


import (
	"html/template"

	"github.com/gin-gonic/gin"
)

type staticMap struct {
	relativePath string
	root         string
}

type APP struct {
	ginEngine   *gin.Engine
	funcMap     template.FuncMap
	templateDir string
	staticDir   staticMap
	urlsList    []GsUrl
	views       View
}

type GsUrl struct {
	urlPath string
	urlName string
	urlView View
}

func NewApp() *APP {
	r := gin.Default()
	a := &APP{
		ginEngine:   r,
		funcMap:     TemplateFilters,
		templateDir: "templates/*",
		staticDir:   staticMap{relativePath: "/internal/static/", root: "./internal/static/"},
	}
	a.funcMap["url"] = a.GetUrls
	r.SetFuncMap(a.funcMap)

	r.LoadHTMLGlob(a.templateDir)
	r.Static(a.staticDir.relativePath, a.staticDir.root)
	return a
}

func (a *APP) Run() {
	a.ginEngine.Run()
}

func (a *APP) RegisterUrl(path, name string, v View) {
	a.urlsList = append(a.urlsList, GsUrl{
		urlPath: path,
		urlName: name,
		urlView: v,
	})
	a.ginEngine.GET(path, gin.HandlerFunc(v))
}

func (a *APP) GetUrls(name string) string {
	for _,v := range a.urlsList {
		if name == v.urlName {
			return v.urlPath
		}
	}
	return  ""
}
