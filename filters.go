package gg

import (
	"fmt"
	"html/template"
	"reflect"
	"strings"
	"time"
)


type filters struct{
	app *APP
}

var TemplateFilters = template.FuncMap{
	"date":     dateFilter,
	"lower":    lowerFilter,
	"upper":    upperFilter,
	"capfirst": capFirstFilter,
	"now":      nowFilter,
	"len":      lengthFilter,
	"default":  defaultFilter,
	// "url":      NewApp().GetUrls,
}

var dateFormats = map[string]string{
	"yyyy-mm-dd": "2006-01-02",
	"dd-mm-yyyy": "02-01-2006",
	"dd/mm/yyyy": "02/01/2006",
	"yyyy/mm/dd": "2006/01/02",
}

func dateFilter(t time.Time, s ...string) string {
	var format string

	if len(s) <= 0 {
		format = "yyyy-mm-dd"
		return fmt.Sprint(t.Format(time.RFC1123Z))

	} else {
		format = s[0]
	}

	v, k := dateFormats[format]
	if !k {
		format = "2006-02-01"
	}
	format = v
	return fmt.Sprint(t.Format(format))
}

func lowerFilter(s string) string {
	return fmt.Sprint(strings.ToLower(s))
}

func upperFilter(s string) string {
	return fmt.Sprint(strings.ToUpper(s))
}

func capFirstFilter(s string) string {
	first := s[0]
	return fmt.Sprint(strings.ToUpper(string(first)) + s[1:])
}

func nowFilter(f ...string) string {
	stdFormat := "yyyy-mm-dd"

	if len(f) > 0 {
		stdFormat = f[0]
		return dateFilter(time.Now(), stdFormat)

	}

	return dateFilter(time.Now())
}

func lengthFilter(s interface{}) int {
	switch reflect.TypeOf(s).Kind() {
	case reflect.Slice, reflect.Ptr:
		values := reflect.Indirect(reflect.ValueOf(s))
		return values.Len()
	}
	return 0
}

func defaultFilter(r, f any) any {
	if f != nil {
		return f
	}
	return r
}
