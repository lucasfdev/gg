package gg

import "github.com/gin-gonic/gin"

type View gin.HandlerFunc

type ViewContent map[string]any

type TemplateView func(path, template_name string, d ViewContent)


